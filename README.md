# explo-mediawiki

Widget **[Datami](https://gitlab.com/multi-coop/datami)** de prévisualisation de pages médiawiki issues des wikis Résilience des territoires, ou fabrique des mobilités.

---

## Résumé

La Fabmob possède un site (https://wiki.lafabriquedesmobilites.fr/) fonctionnant avec un moteur de wiki. Sont référencés sur ce site différents types de ressources : projets, formations, acteurs, etc...

Afin de pouvoir partager et de mettre en valeur toutes ces ressources il a été proposé d'utiliser le _widget open source_ créé par la coopérative numérique **[multi](https://multi.coop)** : le projet **[Datami](https://gitlab.com/multi-coop/datami)**.

Cet outil permet la **visualisation** des données mais également la **contribution**.

**Datami** permet d'intégrer sur des sites tiers (sites de partenaires ou autres) une sélection plus ou moins large de ressources. Cette solution permet à la fois d'éviter aux sites partenaires de "copier-coller" les ressources, d'afficher sur ces sites tiers les ressources toujours à jour, et de permettre aux sites tiers ainsi qu'au site source de gagner en visibilité, en légitimité et en qualité d'information.

L'autre avantage de cette solution est qu'elle n'est déployée qu'une fois, mais que le widget peut être intégré et paramétré/personnalisé sur autant de sites tiers que l'on souhaite... **gratuitement**.

A titre de démonstration de généricité de cette solution nous l'avons déclinée de différentes manières, par exemple en configurant et intégrant ce widget pour qu'il affiche des "communs" référencés sur le wiki [Résilience des territoires](https://wiki.resilience-territoire.ademe.fr).

---

## Démo

[![Netlify Status](https://api.netlify.com/api/v1/badges/c8f1dd33-0f97-401a-b670-5b8cfdc39689/deploy-status)](https://app.netlify.com/sites/demo-explowiki/deploys)

- URL de démo :
  - explowiki / fabmob : https://datami-demo-explowiki.netlify.app/
  - explowiki / résilience des territoires : ...

---

### Documentation 

Un site dédié à la documentation technique de Datami est consultable ici : https://datami-docs.multi.coop

---

## Mini server for local development

A mini server is writen in the `server.py` to serve this folder's files, so we could test and develop locally while running Datami.

### Install mini server

To install the mini-server :

```sh
pip install --upgrade pip
python3 -m pip install --user virtualenv
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

or

```sh
sh setup.sh
source venv/bin/activate
```


### Run local server

To run the server on `http://localhost:8888`:

```sh
python server.py
```

or

```sh
sh run_server.sh
```

Files will be locally served on :

- `http://localhost:8888/content/<path:folder_path>/<string:filename>`
- `http://localhost:8888/statics/<path:folder_path>/<string:filename>`

Check your Datami html page for testing : 

- `http://localhost:8888`

---

## Crédits

| | logo | url |
| :-: | :-: | :-: |
| **ADEME** | ![ADEME](./images/ademe-logo.png) | https://ademe.fr/ |
| **fabmob** | ![fabmob](./images/fabmob-logo.png) | https://lafabriquedesmobilites.fr/ |
| **coopérative numérique multi** | ![multi](./images/multi-logo.png) | https://multi.coop |

---

## Pour aller plus loin

### Datami

Le widget explowiki fait partie intégrante du projet [Datami](https://gitlab.com/multi-coop/datami)

