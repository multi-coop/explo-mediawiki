# Schémas des différentes catégories du wiki

**Liens utiles :**

- [Page mediawiki](https://wiki.lafabriquedesmobilites.fr/wiki/Sp%C3%A9cial:WfExplore?title=Sp%C3%A9cial%3AWfExplore&page=1&wf-expl-Page_creator-fulltext=&wf-expl-Tags=)
- [Tableur Explowikii - lot2](https://docs.google.com/spreadsheets/d/1stw9pkqgW8swVdXbp7NjZUSc3GUbxIlUx6K45yLO0-E/edit#gid=0)

**Catégories :**

- Projets
- Défis
- Véhicules
- Communs
- Référentiels
- Acteurs

---

## Synthèse des clés <> catégories

| clés / catégories    | Projets | Défis | Véhicules | Communs | Référentiels | Acteurs |
|----------------------|:-------:|:-----:|:---------:|:-------:|:------------:|:-------:|
| description          | x       | x     |           | x       | x            | x       |
| shortDescription     | x       |       |           | x       | x            | x       |
| Main_Picture         | x       | x     | x         | x       | x            | x       |
| Tags                 | x       | x     | -         | x       | x            | x       |
| Theme                | x       | x     |           | x       | x            | x       |
| url                  | x       |       | -         | x       | -            | x       |
| challenge            | x       |       |           | x       | x            | x       |
| chat                 | -       |       | -         | x       | -            |         |
| Coordonnées géo      | -       |       | x         |         |              |         |
| to                   | -       |       |           | -       |              |         |
| communauté d'intérêt | x       |       |           | x       | x            |         |
| location             | -       |       |           |         |              |         |
| Coordonnées          | -       |       |           |         |              |         |
| referent             | -       |       |           |         |              |         |
| keyuser              | -       |       |           |         |              |         |
| complement           | x       |       |           | x       |              |         |
| pageCo               | x       |       |           |         |              |         |
| Enjeu                |         | x     |           |         |              |         |
| Caractéristiques     |         | x     |           |         |              |         |
| from                 |         |       |           | -       | -            |         |
| commonscategory      |         |       |           | x       |              |         |
| typeveh              |         |       | x         |         |              |         |
| categveh             |         |       | x         |         |              |         |
| avancement           |         |       | x         |         |              |         |
| challenge veh        |         |       | x         |         |              |         |
| communauté veh       |         |       | x         |         |              |         |
| dossier_veh          |         |       | x         |         |              |         |
| type                 |         |       |           | -       |              |         |
| pays                 |         |       | x         |         |              |         |
| type                 |         |       |           | x       |              |         |
| OpenChallenge        |         |       |           | -       |              |         |
| name                 |         |       |           |         |              | x       |
| communauté           |         |       |           |         |              | x       |
| Richesse             |         |       |           |         |              | x       |

---

## Projets

### Exemples

```txt
Project
|shortDescription=Solution de mobilité électrique partagée
|Main_Picture=autodream.png
|description=Solution de mobilité électrique partagée pour les zones rurales avec des véhicules électriques sans permis en libre-service
|location=Région Centre Val-de-Loire
|Coordonnées géo=47.21802, 2.07837
|url=http://www.autodream.mobi
|Tags=mobilité inclusive, Développement durable, Autopartage, territoire peu dense
|Theme=Autopartage - location courte durée, Collectivité, TENMOD
|referent=BECOULET Philippe
|challenge=Rendre accessible une mobilité individuelle à bas coût pour tous sans externalités négatives,Améliorer les solutions et développer de nouvelles solutions de mobilités pour tous
|communauté d'intérêt=Comité French Mobility,Communauté autour des traces de mobilité et des données associées,Communauté des Territoires et Collectivités,Communauté autour de l'écomobilité inclusive,Communauté des porteurs de projets TENMOD
|keyuser=BECOULET Philippe
|chat=https://chat.fabmob.io/channel/projets_tenmod
|complement=La solution de mobilité s'adresse à tous les citoyens (à partir de l'âge de 16 ans). Elle peut être installée dans tous les territoires en France.
|pageCo=https://www.communecter.org/#@autodream
```

```txt
Project
|shortDescription=Cartographie du réseau de transport artisanal à Accra
|Main_Picture=Accra Mobility.jpg
|description=A Accra, 80% des déplacements motorisés sont assurés par les Trô-Trô. Ces véhicules - des minibus de 10 à 19 passagers - sont exploités par des opérateurs privés qui s'organisent le long de lignes. En 2015, le gouvernement Ghanéen a créé les DoTs (Departement of Transports) au sein de quatre municipalités d'Accra. Ces organismes - notamment en charge de la régulation des taxis et des Trô-Trô au sein de la capitale Ghanéenne - ont procédé à un appel à projet pour cartographier le transport artisanal et ainsi développer les outils nécessaires à l'analyse du fonctionnement du réseau de Trô-Trô. L'objectif second était de développer une approche innovante autour des communs numériques -via l'utilisation d'outils OpenSreetMap- afin de simplifier le processus de collecte, de traitement et de mise en forme de la donnée.
|location=Accra, Ghana
|Coordonnées géo=5.56001, -0.20574
|url=http://data.afd.fr/accramobility/
|Tags=Accra Mobile, Trô-Trô, Transport Artisanal, Jungle Bus, OpenStreetMap, Ghana
|Theme=Open Street Map OSM, Traces de mobilité et des données associées, Logiciel Libre, Africa
|from=Urban Transport Project, Jungle Bus, Transitec, Communauté OpenStreetMap du Ghana, Agence Française de Développement
|to=Departement of Transport
|contributeurs=LAINEZ Florian, CHEVRE Antoine, SADDIER Simon, NYAMADOR Enock Seth
|challenge=Augmenter les connaissances partagées en cartographie et usages des véhicules et réseaux de transports
|othercommon=OSM Trackers, JOSM, OSMose
|othercommon2=OSM2GTFS
|communauté d'intérêt=Communauté autour des traces de mobilité et des données associées
|keyuser=LAINEZ Florian, LEHUBY Noémie
|Develop=Disponible et validé
|chat=https://chat.fabmob.io/channel/0-afrique
|filesharing=https://git.digitaltransport4africa.org/cities/accra

[[Fichier:Accra Mobile Map.jpg|thumbnail|300x300px]]
==Les phases du projet==
Le projet était divisé en trois phases :
* '''Phase 1 & 2''': Les étudiants de l'Université Concordia ont identifié les 300 lignes composant le réseau de transport d'Accra, puis, sur un échantillon de 60 lignes, ils ont mené des études sur la fréquence de départ des lignes. 
* '''Phase 3''': Le bureau d'étude Transitec, avec l'aide de l'équipe Jungle Bus, a travaillé à la formalisation d'un protocole automatisé de collecte et de traitement, avant de procéder à la mise en forme des données décrivant l'offre de transport des Trô-Trô (Carte + GTFS).

==Collecte de la donnée==
Suite à l'identification des lignes lors des phases 1&2 du projet, les contributeurs OpenStreetMap Ghanéens ont pu commencer à enregistrer la position des arrêts et des traces GPS des lignes de Trô-Trô directement sur OpenStreetMap en utilsant OSM Tracker.

==Traitement et publication de la donnée==
La qualité de la donnée était contrôlée tout le long de la collecte par l'équipe Jungle Bus. Ils développèrent un outil de validation sur OSM (Jungle Bus ruleset) qui permet d'en contrôler la qualité. En effet, le plug-in envoie des alertes aux superviseurs de collecte lorsqu'il repère une erreur ou un manque (erreur ou tag non reconnu, manque d'information comme le mode de transport ou le numéro de la ligne...) et lui permet de corriger ces erreurs. Ils ont également travaillé avec l'outils OSMose, un outil de visualisation des erreurs détectées.

Finalement, ils ont créé un jeu de données GTFS à partir des traces GPS extraient d'OSM grâce à l'outil OSM2GTFS et ont produit une carte schématique du réseau de Trô-Trô d'Accra. 
 
==Ressources==
[http://digitaltransport4africa.org/ Centre de ressources Digital Transport 4 Africa]<br>
[[Projets pilotes de cartographie]]<br>[https://git.digitaltransport4africa.org/cities/accra/tree/master/GTFS Jeu de données GTFS]<br>[https://git.digitaltransport4africa.org/cities/accra/tree/master/map Plan du réseau (pdf/svg)]<br>[https://git.digitaltransport4africa.org/cities/accra/tree/master/papers Documentations]<br>[http://data.afd.fr/accramobility/ Site Internet]


'''Autres langues :''' [[Fichier:EN_Flag.png|20x20px]]  '''[[Accra Mobility|English]]''' - [[Fichier:FR_Flag.png|20x20px]] Français
```

---

## Défis

### Exemples

```txt
Défi
|Enjeu=Il s'agit de produire des ressources à faibles barrières à l'entrée pour expérimenter des idées nouvelles sur un véhicule homologué ou un véhicule "support" capable de réduire les barrières à l'entrée pour de nouveaux acteurs!
|description=Cela passe par plusieurs possibilités :
* produire une base véhicule ouverte et documentée avec plusieurs versions et plusieurs capacités d'utilisation, versatile, associée à des communautés d'acteurs : des entrepreneurs, étudiants pourraient tester leurs idées de façon la plus simple et rapide possible, avec le minimum d'autorisation à demander, 
* produire des composants véhicules standards ouverts et documentés permettant de les modifier, adapter,
|Caractéristiques=Les solutions devront être bien documentées, associées à une bibliothèque de cas d'usages possibles (véhicule non utilisable sur voie publique, véhicule limité à 5 km/h, ...). Une attention particulière devra être apportée aux différentes communautés qui pourront utiliser, contribuer, documenter, hacker ces ressources.
|Tags=véhicule, automobile
|Theme=Voiture Connectée, Voiture électrique et charge, Carburants alternatifs, Données ouvertes, Open HardWare, Ecoles et Etudiants, Autopartage - location courte durée
|Main_Picture=fabmob-cup.jpg
* ce défi a été identifié pour l'Open Challenge.
```

```txt
Défi
|Enjeu=Il s'agit de rendre nos systèmes de transports et plus largement nos activités quotidiennes plus résilientes à des stress de plusieurs natures : énergétique, financier, épidémiologique, catastrophe naturelle, ...
|description=Cela touche à la fois les modes de transports, mais également les infrastructures physiques et numériques, les systèmes d'information, mais également les modes d'organisation individuels et collectifs nous permettant de réaliser nos activités quotidiennes en réduisant nos besoins de transports et/ou en changeant nos pratiques de mobilités.
|Caractéristiques=Les projets visant à résoudre ce défi sont à la fois de nature technologique mais également organisationnel. Ils devront à viser à développer des solutions plus résilientes, sobres, réparables, produites et maintenables localement, plus efficientes. Toutes les solutions devront être moins gourmandes en ressources, en besoin d'infrastructures physiques ou numériques.
|Tags=résilience, COVID
|Theme=Open HardWare, Vélo et Mobilités Actives, Données ouvertes, Traces de mobilité et des données associées, Covoiturage quotidien, Ecoles et Etudiants, Urbanisme et ville, Logistique urbaine, Conseillers en mobilité, Collectivité, Centre Excellence Québec
|Main_Picture=Fabmob-cup.jpg
```

---

## Véhicules

### Exemples

```txt
Vehicule
|fabricant=Acticycle Start-up
|modeleveh=Acticycle
|contact=Olivier CORNET
|Main_Picture=Acticycle Concept Wiki XD.PNG
|Main_Picture2=Acticycle Vue Wiki XD.PNG
|url=https://acticycle.fr/
|pays=FR
|Coordonnées géo=45.75781, 4.83201
|avancement=prototype
|typeveh=VAE,velo voiture
|categveh=VAE
|nbpers=1+2 enfants
|nbrouearr=2
|nbroueav=2
|volumecoffre=40 à 80 litres sur POC actuel
|propulse=assistance electrique
|transmission=chaine
|direction=bras leviers
|freinage=disque
|chassis=alu
|Tags=XD1
|challenge veh=L'extrême défi ADEME
|communauté veh=Communauté de l'extrême défi
|dossier_veh=Notre concept de véhicule :

'''Acticycle''' est un '''mode de transport actif & doux''' pour petit trajets urbain & péri-urbain :

*'''Carrosserie'''/canopée '''amovible,''' mixte rigide et textile '''étanche''' au ruissèlement
*'''Confortable et ergonomique :''' tout suspendu avec assistance électrique de type vélo cargo, une position de conduite assise proche d’une voiture qui permet de développer plus de puissance au pédalage.
*'''Sécurisant :''' avec 4 roues stable, des portes en option, '''a'''utorisé sur '''piste cyclable.'''
*'''Autonomie ~50 à 100 kms''' avec 1 batterie amovible (2 possibles) '''& panneau solaire''' en option.
*'''Puissance''' équivalente à un vélo cargo à assistance électrique
*Véhicule '''faible émission CO2''' à l’usage, '''20x''' plus léger qu’une voiture.
*Un budget compétitif : véhicule '''sans permis, entretien réduis''' comme un vélo cargo avec un prix d’attaque inférieur à une voiturette.
*'''Livraison en kit''' à l’étude.

POC actuel monoplace. Prévu en bi-place du type tandem. '''Résolument « low-tech »''' afin de limiter les investissements spécifiques et l’empreinte environnementale. Une réflexion est en cours avec l'équipe pour que la version bi-place tandem soit polyvalente afin d'accueillir 2 enfants côte à côte au lieu d'un adulte ou bien recevoir un chargement pour la livraison.
|fichier_veh=20221020 Dossier XD BOM Acticycle-b.pdf
|dossier_nrj=L'origine du projet ACTICYCLE est essentiellement une '''préoccupation environnementale''' croisée d’une conviction que l’assistance électrique qui se développe sur les VAE décuple les possibilités de se déplacer confortablement de façon nettement plus vertueuse, sans négliger le bénéfice santé d’une mobilité active quotidienne.

La masse en mouvement est un élément clé de la problématique. L'automobile actuelle pèse de plus en plus lourd et offre des accélérations flatteuses, surtout en électrique qui deviennent déconnecté des besoins réels et des usages. En 2019 en passant au '''POC monoplace''' ACTICYCLE a voulu montrer qu’un véhicule '''20 fois''' '''plus léger''' qu’une voiture était possible, que l’'''énergie nécessaire''' au déplacement pouvait être '''drastiquement réduite''' sans sacrifier au plaisir de petits déplacements.

En terme d'énergie : notre concept utilise un '''moteur pédalier''' du même type que les '''VAE cargos'''.

'''En saison 1''' =  '''réalisation des''' '''ACV''' (analyses de cycle de vie) comparatives appliquées au gabarit '''de notre véhicule''' en monoplace et bi-place. Identification de la part fin de vie et recyclage au regard des consommations en ressources, notamment vis-à-vis des nouvelles directives renforcées sur les véhicules hors d’usage.
|fichier_nrj=20221020 Dossier XD NRJ-ACV Acticycle.pdf
```

```txt
Vehicule
|fabricant=Daymak Avvenire
|modeleveh=Aspero
|Main_Picture=aspero1.PNG
|url=https://daymakavvenire.com/products/aspero/
|pays=CA
|Coordonnées géo=50.00068, -86.00098
|avancement=concept
|typeveh=voiturette
|nbpers=1
|nbrouearr=2
|nbroueav=2
|propulse=electrique
|challenge veh=L'extrême défi ADEME
|communauté veh=Communauté de l'extrême défi
```

---

## Communs

### Exemples

```txt
Ressource
|Lang=English
|shortDescription=ADVISOR Advanced Vehicle Simulator
|Main_Picture=Advisor.jpg
|url=http://adv-vehicle-sim.sourceforge.net/
|description=This project holds the latest releases for canonical versions of the ADVISOR® Software and "Advanced Vehicle Simulator". ADVISOR is a MATLAB/Simulink based simulation program for rapid analysis of the performance and fuel economy of light and heavy-duty vehicles with conventional (gasoline/diesel), hybrid-electric, full-electric, and fuel cell powertrains. New features are developed under the title "Advanced Vehicle Simulator". Periodically, stable versions of Advanced Vehicle Simulator will be submitted for consideration as the next canonical version of the ADVISOR software.
|Tags=vehicule, simulation
|commonscategory=Logiciel, Communauté
|challenge=Abaisser les barrières pour innover sur le véhicule, L'extrême défi ADEME
|communauté d'intérêt=Communauté Voiture Connectée, Communauté du Logiciel Libre, Communauté de l'extrême défi
|chat=https://chat.fabmob.io/channel/open_logiciel
|complement=For online help with ADVISOR please visit [https://groups.google.com/group/adv-vehicle-sim the ADVISOR Community Group].
|type=Commun
```

```txt
Ressource
|shortDescription=WebService utilisé pour récupérer les trajets de covoiturage correspondant aux critères renseignés
|Main_Picture=VianavigoAPI.jpg
|url=https://doc.vianavigo.com/api-carpool/#/
|description=Cet API permet de renvoyer des propositions de covoiturage de plusieurs opérateurs sur l'application vianavigo.

[https://www.vianavigo.com/covoiturage Les services de covoiturage en Ile-de-France]

Depuis le 1er octobre 2017, 8 services de covoiturage ont développé une interface pour référencer leurs trajets sur Vianavigo. Ainsi, quand vous effectuez votre recherche d’itinéraire, dès qu’un numéro s’affiche sur l’ongletCovoituragec’est qu’un conducteur d’un des services réalise un trajet qui correspond à votre demande. Après avoir cliqué sur l’onglet, la liste de trajet(s) vous précise à chaque fois l’itinéraire emprunté du trajet, l’horaire de départ et d’arrivée, le prix et également le nombre de places disponibles dans le véhicule du conducteur. Après avoir sélectionné le trajet qui vous convient le mieux, il vous suffit de cliquer sur « réserver » pour vous rendre sur le site de l’opérateur.
|Tags=API, Covoiturage
|commonscategory=Logiciel
|Theme=Covoiturage quotidien
|from=IDF Mobilités
|to=Le centre d’excellence des communs technologiques pour la mobilité
|challenge=Accélérer le déploiement du covoiturage quotidien
|communauté d'intérêt=Communauté autour du Covoiturage quotidien
|type=Commun
|Develop=Disponible et validé
|chat=https://chat.fabmob.io/channel/covoiturage_quotidien
|OpenChallenge=Non
```

```txt
Ressource
|shortDescription=Simplifier l'accès aux données et démarches administratives du transport routier de marchandises
|Main_Picture=A-dock.jpg
|url=https://beta.gouv.fr/startups/a-dock.html
|description=== Le constat initial : le transport routier de marchandises, un secteur exposé à des risques de déréglementation ==
Le transport routier de marchandises en France (TRM) - 40 000 entreprises, 400 000 salariés -  est historiquement et structurellement exposé à des risques de déréglementation. En effet, il est :
* réglementé mais hétérogène, avec une nette dispersion des entreprises : ''+50% de TPE-PME''.
* impacté par une concurrence européenne forte et déséquilibrée, du fait d’une harmonisation européenne faiblement effective : ''45% des transports intérieurs sont réalisés par des acteurs étrangers''.
* marqué par l’essor des Véhicules Utilitaires Légers (VUL), non réglementés : ''20% du transport intérieur''.
L’apparition de nouveaux acteurs issus du numérique (plateformes d’intermédiation), hors du champ réglementaire, vient défier les pratiques d’un secteur dont la transition numérique n’est pas encore faite.

'''La captation des données professionnelles des transporteurs par les plateformes renforce les risques de déréglementation.'''

Le transport routier peut ainsi subir une transformation du seul fait des GAFAM, qui, comme ils l’ont fait dans d’autres secteurs, imposeront leurs règles et orientations avec des impacts économiques, sociaux et environnementaux non maîtrisés (on reconnait là le ''phénomène d’uberisation'').

L’Etat est en mesure d’accompagner cette transformation, de la réglementer et la réguler, en créant une infrastructure numérique et, plus précisément, en en impulsant la création d’un “commun” ou catalogue de données du TRM, constitué, alimenté et utilisé par l’ensemble des  acteurs, selon des règles communes.

== La solution : A Dock, la plateforme numérique fer de lance du ''“commun”'' du transport routier de marchandises ==
Depuis Janvier 2018, développée par la Direction Générale des Infrastructures, des Transports et de la Mer (DGITM), avec l’appui de la Fabrique Numérique, incubateur du Ministère de la transition écologique et solidaire,  A Dock se propose d’être la plateforme permettant d’impulser le “commun” du transport routier de marchandises :

''En valorisant les données déjà détenues par l’Etat'' (données Sirene, registre électronique national des entreprises de transport par route) ''ou par des acteurs para-publics'' (ADEME, Programme Eve).

''En collectant de nouvelles données via une offre de services aux acteurs du transport routier'' (transporteurs, chargeurs, commissionnaires).

== La vision pour la plateforme : A Dock, le point d’accès unique aux données et démarches du transport routier de marchandises ==
A Dock ambitionne d’être le point d’accès public national vers l’ensemble des données (''commun du TRM'') et des démarches administratives essentielles du secteur. À cet égard, le service est développé en collaboration avec ses utilisateurs cibles (transporteurs routiers, chargeurs, commissionnaires), leurs organisations professionnelles, ainsi que les administrations centrales ou régionales compétentes (DREAL), afin d’être au plus près de leurs attentes et besoins.

Le Github : https://github.com/MTES-MCT/adock-api
|Tags=transport marchandise, TRM, transport de marchandises
|commonscategory=Logiciel, Données
|Theme=Traces de mobilité et des données associées, Logistique urbaine, Logiciel Libre
|from=Beta gouv
|chat=https://chat.fabmob.io/channel/open_logistic
```

---

## Référentiels

### Exemple

```txt
Connaissance
|shortDescription=Cette page décrit les actions prises par des villes et autorités organisatrices aux sujets des transports pour lutter contre le COVID, notamment sur le sujet de la distanciation.

'''Autres pages liées au COVID :'''
* [[Données sur l'impact du coronavirus sur la mobilité|Pour les données générales liées à l'impact du COVID sur les mobilités]]
* [[Traceurs de Mobilité et COVID19|Pour les projets liés au tracking et tracing des mobilités liés au COVID]]
|Main_Picture=Covid-metro.jpg
|description=...
== Questionnaire pour les collectivités qui s'engagent ==
* Nous invitons les collectivités qui engagent des travaux d'aménagement liés à l'amélioration de la distanciation à les décrire en suivant ces questionnaires rapides : https://pad.fabmob.io/distanciation

== Guide et FAQ pour les collectivités qui souhaitent se lancer ==
* Nous invitons les collectivités à décrire leur travaux dans leur SIG en suivant les standards numériques pour décrire les aménagements temporaires en rejoignant https://schema.data.gouv.fr/metis-reseaux/infos-travaux/latest.html
* Guide du CEREMA [https://www.cerema.fr/fr/actualites/quels-amenagements-pietons-lors-phase-deconfinement-0 Quels aménagements pour les piétons lors de la phase de déconfinement ?]
* [[Recommandations pour la création et l’extension temporaires des infrastructures cyclables|Pour les actions et recommandations liées aux vélos]]
* Le gouvernement fournit des [https://www.ecologique-solidaire.gouv.fr/faq-covid-19-transports-et-deconfinement recommandations sanitaires] et renvoie au décret général sur les mesures générales face au COVID-19 (Décret n° 2020-293 du 23 mars 2020).  

== Bases de données ==
* NACTO’s City Transportation Action Updates will be continually updated with direct actions taken by cities and transit agencies globally. To submit actions from your department or agency please email covid19@nacto.org. NACTO will be updating this daily : https://nacto.org/program/covid19/  et [https://docs.google.com/spreadsheets/d/1xBf6gMAwNSzNTr0-CbK_uTA0ZapWGiOP58Dfn6qeC6Y/edit#gid=89542582 Accès au Google Sheet]
* [https://www.covidmobilityworks.org Base de données des démarches mises en oeuvre post Covid], [https://www.numo.global/news/resources-responses-covid-19-live Base de données mondiale des ressources et projets (NUMO)] et '''[https://docs.google.com/spreadsheets/d/e/2PACX-1vTAeD_Hiz4rc9FqriO5aQClch8q_XvmGVYrvlBMAq7Qy7SkNGRjHUAbyzkKmuq0M3yHdwWTMvOQ059u/pubhtml?widget=true&amp;headers=false lien direct vers le Google Sheet''']. 

== Projets engagés ==
* [http://umap.openstreetmap.fr/nl/map/bike-sharing-during-corona-lockdown_448784#5/46.438/3.999 Services de Vélos partagés disponibles durant le COVID]
* Prêtons nos vélos au personnel soignant et aidons-les à se déplacer plus facilement : https://www.desvelospourlhosto.fr/
* '''[https://www.facebook.com/lescoursierssolidairesduvaucluse/ Les coursiers solidaires du Vaucluse]''' aident bénévolement les personnes fragiles en s'occupant de leurs courses afin de leur éviter d'entrer en contact avec le virus (voir [https://www.francebleu.fr/infos/societe/coronavirus-les-coursiers-solidaires-du-vaucluse-pedalent-pour-faire-les-courses-des-personnes-agees-1586272200 article France Bleu] ainsi que la [https://www.google.com/maps/d/viewer?mid=13l_1BC4pTMsdonxWovn3WZ_yvQEFizT-&ll=43.931155517853156%2C5.1488974999999755&z=10 carte des volontaires]).
|Tags=COVID, covid, transports publics
|Theme=Open Street Map OSM, Vélo et Mobilités Actives, Accessibilité dans les transports, Traces de mobilité et des données associées, Covoiturage quotidien, Autopartage - location courte durée, Urbanisme et ville, Logistique urbaine, Stationnement, Voiture électrique et charge, Collectivité
|challenge=Améliorer la résilience des systèmes de transports et des solutions de mobilités
|communauté d'intérêt=Communauté des Territoires et Collectivités, Communauté autour de l'accessibilité dans les transports, Communauté du Stationnement
```

```txt
Connaissance
|shortDescription=Découvrir de l’intérieur le monde des ateliers vélos en territoires peu denses et vous donne les outils pour que demain ces ateliers existent aussi sur votre territoire
|Main_Picture=Atelier velo.PNG
|description=Les ateliers participatifs et solidaires de réparation de vélos ne riment pas forcément avec milieu urbain. L’Heureux Cyclage, en association avec La Ferme à Cycles et Mobil’idées, et soutenue par l’ADEME, la MAIF, et le bureau d’études ACUM, a organisé un temps de travail en janvier 2020 pour questionner les acteur·ices des ateliers vélos en territoires peu denses.

Cet ouvrage vous invite à découvrir de l’intérieur le monde des ateliers vélos en territoires peu denses et vous donne les outils pour que demain ces ateliers existent aussi sur votre territoire.

Bienvenue dans le monde du partage des savoirs, du réemploi des objets, de l’entraide et de la promotion du vélo, bienvenue dans l’économie sociale et solidaire.
[https://cloud.fabmob.io/s/DFMcbRjxTG7wGXT '''Lien vers le document''']
|url=https://www.heureux-cyclage.org/ateliers-velo-en-territoires-peu.html
|Tags=atelier, vélo, territoire peu dense
|Theme=Vélo et Mobilités Actives, Ecomobilité scolaire, Collectivité
|from=Heureux Cyclage
|challenge=Améliorer les solutions et développer de nouvelles solutions de mobilités pour tous
|communauté d'intérêt=Communauté Vélo et Mobilités Actives, Communauté des Territoires et Collectivités
|chat=https://chat.fabmob.io/channel/velo_mob_actives
```

```txt
Connaissance
|Lang=Français
|shortDescription=il s'agit de regrouper les ressources documentaires sur les communs
|Main_Picture=bibliographie-des-communs.png
|description='''1- Projet de recherche-action : A l’interface entre l’État et le marché, des dynamiques de bien commun catalyseurs de la transition énergétique dans les territoires ?'''
*Pour une approche pragmatique des communs, illustrée par de nombreux exemples : David Bollier, 2014, La renaissance des communs, Ed. Charles Léopold Mayer. Traduit de l’américain par Olivier Petitjean, 192 pages, ISNB 978-2-84377-182-8 / L’ouvrage est disponible en version brochée, ou en PDF en accès libre sur le site des ed. Charles Léopold Mayer : http://www.eclm.fr/ouvrage-364.html  . Vous trouverez également une courte note de lecture sur cet ouvrage ici : https://www.erudit.org/fr/revues/recma/2015-n335-recma01695/1028538ar.pdf

*Sur les communs de la connaissance (musique, logiciel libre, innovation) : Benjamin Coriat (dir.), 2015, Le retour des communs : la crise de l’idéologie propriétaire, Paris, Les Liens qui Libèrent, 297 pages
Le site de l’éditeur : http://www.editionslesliensquiliberent.fr/livre-Le_retour_des_communs-9791020902726-1-1-0-1.html / Vous trouverez également une note de lecture ici : https://journals.openedition.org/regulation/11549

*Pour une approche philosophique et politique des communs : Pierre Dardot, Christian Laval, Commun. Essai sur la révolution au XXIe siècle, Paris, La Découverte, 2014, 600 p., EAN : 9782707169389.
Le site de l’éditeur : https://www.editionsladecouverte.fr/catalogue/index-Commun-9782707169389.html
Vous trouverez également une note de lecture ici : https://laviedesidees.fr/Le-commun-contre-l-Etat-neoliberal.html

*Les travaux d’Elinor Ostrom, qui ont été le point de départ du renouveau de la réflexion sur les communs :
Elinor Ostrom, Gouvernance des biens communs - Pour une nouvelle approche des ressources naturelles, Ed. de Boeck, 2010, 302 p.  Le site de l’éditeur : https://www.deboecksuperieur.com/ouvrage/9782804161415-
gouvernance-des-biens-communs
Une note de lecture ici : https://blog.mondediplo.net/2012-06-15-Elinor-Ostrom-ou-la-reinvention-des-biens-communs

*Pour une approche plus juridique des communs / Parance Béatrice, De Saint Victor Jacques (dir.), Repenser les biens communs, Paris, CNRS éditions, 2014, 314 pages. Site de l’éditeur : http://www.cnrseditions.fr/economie-droit/6849-repenser-les-bienscommuns-sous-la-direction-de-jacques-de-saint-victor-et-de-beatrice-parance.html
Note de lecture : https://journals.openedition.org/developpementdurable/11004

*Sur les communs et l’entreprise, une réflexion du point de vue de la recherche en management :
Bommier S., Renouard C. (2018), L’entreprise comme commun. Au-delà de la RSE, Paris, Charles Léopold Mayer, Coll. Essai L’ouvrage est disponible en version brochée, ou en PDF en accès libre sur le site des ed.
Charles Léopold Mayer : http://www.eclm.fr/ouvrage-394.html Une note de lecture sur l’ouvrage :
https://www.researchgate.net/publication/326205708_Bommier_S_Renouard_C_2018_L'entreprise_comme_commun_Au-dela_de_la_RSE_Paris_Charles_Leopold_Mayer_Coll_Essai

*Le colloque « Entre État et marché, la dynamique du commun : vers de nouveaux équilibres » (Paris II, 8-10 juin 2017)  Les matériaux du colloque sont disponible sur le site du Fund for Democratic Culture :
http://fundfordemocraticculture.org/presentations-et-videos-colloque-etat-marche-commun/
Ce colloque va prochainement donner lieu à un ouvrage collectif, sous le titre Dynamiques du commun : État, Marché et Société, dirigé par Danièle Bourcier, Jacques Chevallier, Gilles Hériard Dubreuil, Sylvain Lavelle et Emmanuel Picavet
|url=https://annuaire.lescommuns.org/category/livres/
|Tags=commun, bibliographie
|from=Point Communs
|referent=MaiaDereva
|challenge=Faire progresser la FabMob
|chat=https://chat.fabmob.io/channel/communs
```

---

## Acteurs

### Exemple

```txt
Acteur
|name=11.100.34. Avocats associés
|Main_Picture=Logo-avocat.png
|description=Convaincu qu'il existe une stratégie juridique entrepreneuriale, comme il existe une stratégie commerciale et financière, 11.100.34. Conseille et accompagne les entrepreneurs dans tous les secteurs de l'innovation technologique, sociale et culturelle. 
Le cabinet propose également son expertise à de jeunes entrepreneurs et créateurs d’entreprise, depuis leur création jusqu’à leurs levées de fonds, dans le cadre de la "Startup Box".
|Tags=Juridique
|Theme=Conseillers en mobilité
|members=CANTONI Thomas
```

```txt
{Acteur
|name=ADULLACT
|Main_Picture=ADULLACT.jpg
|shortDescription=Fondée en 2002, l'ADULLACT a pour objectifs de soutenir et coordonner l'action des Administrations et Collectivités territoriales dans le but de promouvoir, développer et maintenir un patrimoine de logiciels libres utiles aux missions de service public.
|description=L’ADULLACT, structure unique en Europe, est une initiative née de la nécessité de voir apparaître une alternative au système des licences propriétaires, en particulier dans le domaine des logiciels métiers. 

En mettant en place des projets informatiques libres répondant aux besoins précis de ses adhérents et en coordonnant les compétences territoriales, l’ADULLACT souhaite donner un sens concret à l’idée de mutualisation des ressources. Avec une conviction et un principe simples : l'argent public ne doit payer qu'une fois !
|Tags=Logiciel libre, Collectivité
|Theme=Logiciel Libre, Collectivité
|url=https://adullact.org/
|pays d'implantation=France
|type=Association
|referent=Pascal Kuczynski
|challenge=Accompagner une collectivité à ouvrir un maximum de ressources et construire un kit d'aide à l'innovation
|communauté=Communauté du Logiciel Libre, Communauté des Territoires et Collectivités
|chat=https://chat.fabmob.io/channel/open_logiciel
```

```txt
Acteur
|Lang=Français
|Main_Picture=Image_Site_Logo.jpg
|shortDescription=Agence Nationale de Cohésion des Territoires
|description='''Annoncée par le président de la République lors de la Conférence nationale des territoires en 2017 et créée par la loi du 22 juillet 2019, l’Agence nationale de la cohésion des territoires (ANCT) a été  mise en place le 1er janvier 2020.'''

Née de la fusion du Commissariat général à l’égalité des territoires, d’Epareca et de l’Agence du numérique, l’ANCT est un nouveau partenaire pour les collectivités locales. Sa création marque une transformation profonde de l’action de l’État : une action désormais plus en lien avec les collectivités territoriales pour faire réussir leurs projets de territoires.

==SES MISSIONS==
L’Agence nationale de la cohésion des territoires assure d’abord un rôle de « fabrique à projets » pour permettre aux collectivités de mener à bien leurs projets.

===Concrétiser les projets de territoire===
L’Agence nationale de la cohésion des territoires facilite l’accès des collectivités locales aux ressources nécessaires pour concrétiser leurs projets : ingénierie technique et financière, partenariats, subventions…

===S’adapter au plus près des besoins===
L’Agence nationale de la cohésion des territoires prend en compte les différences entre les territoires et adapte son action à leurs besoins.

===Faire face aux nouveaux défis===
Transitions numérique, écologique, démographique…, l’Agence nationale de la cohésion des territoires développe des programmes d’appui innovants pour répondre, en lien avec les élus, aux nouveaux enjeux et renforcer la cohésion des territoires.

==Territoires d'industrie==
Lancé par le Premier ministre à l’occasion du Conseil national de l’industrie, le 22 novembre 2018, le programme national « '''[https://agence-cohesion-territoires.gouv.fr/territoires-dindustrie-44 Territoires d’industrie]''' » est une '''stratégie de reconquête industrielle par les territoires'''.
|Theme=Véhicules intermédiaires,eXtrême Défi,Collectivité
|url=https://agence-cohesion-territoires.gouv.fr/lagence-21
|date de création=2020
|pays d'implantation=France
|Coordonnées géo=46.60335, 1.88833
|type=Agence, Partenaire
|needs2='''Apports'''

*Incuber une équipe,
*Mentorer une équipe,
*Réseau des Territoires d'Industrie,
*Financement Territoires d'Industrie
|challenge=L'extrême défi ADEME
|Richesse=Conseil Expertise, Financement
|communauté=Communauté de l'extrême défi
```